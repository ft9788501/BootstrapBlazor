﻿using Microsoft.AspNetCore.Components;

namespace BootstrapBlazor.WebConsole.Pages.Components
{
    /// <summary>
    /// 
    /// </summary>
    sealed partial class AttributeTable
    {
        /// <summary>
        /// 
        /// </summary>
        [Parameter]
        public string Title { get; set; } = "Attributes 属性";
    }
}
